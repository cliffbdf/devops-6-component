package wegood;

import static spark.Spark.*;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.time.format.DateTimeParseException;
import com.google.gson.Gson;

public class WeGood {

	private LocalTime time = null;

	public static void main(String[] args) {
		WeGood weGood = new WeGood();

		//System.out.println(helloWorld.sayHi());
		get("/arewegood", (req, res) -> {
			boolean areWeGood = weGood.areWeGood();
			AreWeGoodResponse response = new AreWeGoodResponse(areWeGood);
			Gson gson = new Gson();
			String json = json = gson.toJson(response);
			return json.toString();
		});

		get("/settime", (req, res) -> {

			LocalTime time;

			String timestr = req.queryParams("time");
			if (timestr == null) {
				throw new Exception("Missing time parameter");
			}

			try {
				time = LocalTime.parse(timestr);
			} catch (DateTimeParseException ex) {
				throw new Exception("time parameter:", ex);
			}

			weGood.setTime(time);

			SetTimeResponse response = new SetTimeResponse();
			Gson gson = new Gson();
			String json = json = gson.toJson(response);
			return json.toString();
		});
	}
	public boolean areWeGood() {
		int hourOfDay = time.get(ChronoField.HOUR_OF_DAY);
		return ((hourOfDay >= 18) || (hourOfDay < 9));
	}

	public void setTime(LocalTime newTime) {
		this.time = newTime;
	}

	static class AreWeGoodResponse {
		public AreWeGoodResponse(boolean good) {
			this.areWeGood = good;
		}
		public boolean areWeGood;
	}

	static class SetTimeResponse {
	}
}
