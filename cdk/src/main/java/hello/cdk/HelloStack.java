package hello.cdk;

import software.amazon.awscdk.services.ecs.Cluster;
import software.amazon.awscdk.services.ecs.ContainerImage;

import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedTaskImageOptions;

import software.amazon.awscdk.services.ec2.Vpc;

import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.StackProps;

public class HelloStack extends Stack {

    public HelloStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    public HelloStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

		/* Create VPC with a max of two availability zones: */
		Vpc vpc = Vpc.Builder.create(this, "hello-vpc")
			.maxAzs(2)  // Default is all AZs in region
			.build();

		/* Create ECS fargate-backed cluster within the VPC created above: */
		Cluster cluster = Cluster.Builder.create(this, "hello-cluster")
			.vpc(vpc)
			.build();

		/* Create a load-balanced Fargate service for the hello microservice and
			make it public: */
		ApplicationLoadBalancedFargateService applicationLoadBalancedFargateService_hello =
			ApplicationLoadBalancedFargateService.Builder.create(this, "hello-service")
			//.serviceName("hello-service")
			.cluster(cluster)           // Required
			.memoryLimitMiB(2048)       // Default is 512
			.cpu(512)                   // Default is 256
			.desiredCount(1)            // Default is 1
			.taskImageOptions(
				ApplicationLoadBalancedTaskImageOptions.builder()
				.image(ContainerImage.fromRegistry("cliffberg/hello"))
				.containerPort(4567)
				//.containerName("hello")
				.build())
			.publicLoadBalancer(true)   // Default is false
			.build();
    }
}
